<?php

namespace App\Service;

use App\Entity\Customer;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;


class CustomerValidatorTest extends TestCase
{
    public ObjectManager $objectManager;
    public ObjectRepository $repository;
    public Customer $customer;
    public CustomerValidator $validator;

    public function setUp(): void
    {
        $this->repository = $this->createMock(ObjectRepository::class);
        $this->objectManager = $this->createMock(ObjectManager::class);
        $this->customer = new Customer();
        $this->validator = new CustomerValidator($this->objectManager);

        $this->customer->setFirstname('John')
            ->setSurname('Doe')
            ->setAddress('Main street 21');

        $this->repository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($this->customer);

        $this->objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($this->repository);
    }

    public function testReturnTrueIfCustomerExists()
    {
        $existingCustomer = $this->customer;

        $result = $this->validator->exists($existingCustomer);
        $expected = true;

        //Assert
        $this->assertEquals($expected, $result);
    }

    public function testReturnFalseIfCustomerNotExists()
    {
        $notExistingCustomer = new Customer();
        $notExistingCustomer->setFirstname('Mike')
            ->setSurname('Tyson')
            ->setAddress('Downing street 21');

        $result = $this->validator->exists($notExistingCustomer);
        $expected = false;

        //Assert
        $this->assertEquals($expected, $result);
    }

}
