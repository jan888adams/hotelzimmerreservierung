<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTimeInterface $arrivalDate;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTimeInterface $departureDate;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Customer $customer;

    /**
     * @ORM\ManyToOne(targetEntity=RoomType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?RoomType $roomType;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getArrivalDate(): ?DateTimeInterface
    {
        return $this->arrivalDate;
    }

    /**
     * @param DateTimeInterface $arrivalDate
     * @return $this
     */
    public function setArrivalDate(DateTimeInterface $arrivalDate): self
    {
        $this->arrivalDate = $arrivalDate;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDepartureDate(): ?DateTimeInterface
    {
        return $this->departureDate;
    }

    /**
     * @param DateTimeInterface $departureDate
     * @return $this
     */
    public function setDepartureDate(DateTimeInterface $departureDate): self
    {
        $this->departureDate = $departureDate;

        return $this;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer|null $customer
     * @return $this
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return RoomType|null
     */
    public function getRoomType(): ?RoomType
    {
        return $this->roomType;
    }

    /**
     * @param RoomType|null $roomType
     * @return $this
     */
    public function setRoomType(?RoomType $roomType): self
    {
        $this->roomType = $roomType;

        return $this;
    }
}
