<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use ArrayObject;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $address;

    /**
     * @var Booking[]
     */
    private array $bookings;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->bookings = [];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return $this
     */
    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Booking[]
     */
    public function getBookings(): array
    {
        return $this->bookings;
    }

    /**
     * @param Booking[] $bookings
     */
    public function setBookings(array $bookings): self
    {
        $this->bookings = $bookings;

        return $this;
    }

    /**
     * @param Booking $booking
     */
    public function addBookings(Booking $booking)
    {
        $this->bookings[] = $booking;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->firstname .' '. $this->surname;
    }

}
