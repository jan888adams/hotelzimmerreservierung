<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use App\Service\BookingValidatorInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reservierungen", name="booking.")
 */
final class BookingController extends AbstractController
{
    private const BOOKING_SUCCESS_MESSAGE = 'sie konnten erfolgreich die Buchung tätigen';
    private const BOOKING_NOT_VALID_MESSAGE = 'diese buchung ist nicht möglich';

    /**
     * @ORM\Column(type="string")
     */
    private ObjectRepository $repository;

    /**
     * BookingController constructor.
     * @param BookingRepository $repository
     */
    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/buchen", name="addBooking")
     * @param Request $request
     * @param BookingValidatorInterface $validator
     * @return Response
     */
    public function addBooking(Request $request, BookingValidatorInterface $validator): Response
    {
        $message = '';

        $booking = new Booking();

        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            dd($validator->isValidArrivalDate($booking->getArrivalDate()));

            if ($validator->isValidArrivalDate($booking->getArrivalDate())
                && $validator->isValidDepartureDate($booking->getArrivalDate(), $booking->getDepartureDate())) {

                $dbManager = $this->getDoctrine()->getManager();
                $dbManager->persist($booking);
                $dbManager->flush();

                $message = self::BOOKING_SUCCESS_MESSAGE;
            } else {
                $message = self::BOOKING_NOT_VALID_MESSAGE;
            }
        }

        return $this->render(
            'booking/form.html.twig',
            [
                'message' => $message,
                'bookingForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/anzeigen", name="showBookings")
     * @return Response
     */
    public function showBooking(): Response
    {
        $bookings = $this->repository->findAll();

        return $this->render('booking/table.html.twig', ['bookings' => $bookings]);
    }

    /**
     * @param BookingRepository|ObjectRepository $repository
     * @return BookingController
     */
    public function setRepository($repository): BookingController
    {
        $this->repository = $repository;

        return $this;
    }
}