<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Customer;
use App\Form\CustomerType;
use App\Repository\BookingRepository;
use App\Repository\CustomerRepository;
use App\Service\CustomerValidator;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/kunden", name="customer.")
 */
final class CustomerController extends AbstractController
{

    const REGISTRATION_SUCCESS_MESSAGE = 'sie konnten sich erfolgreich registrieren';

    /**
     * @ORM\Column(type="string")
     */
    private ObjectRepository $repository;

    /**
     * CustomerController constructor.
     * @param CustomerRepository $repository
     */
    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/registrieren", name="addCustomer")
     * @param Request $request
     * @param CustomerValidator $validator
     * @return Response
     */
    public function addCustomer(Request $request, CustomerValidator $validator): Response
    {
        $message = '';

        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$validator->exists($customer)) {
                $dbManager = $this->getDoctrine()->getManager();
                $dbManager->persist($customer);
                $dbManager->flush();
            }

            $message = self::REGISTRATION_SUCCESS_MESSAGE;
        }

        return $this->render(
            'customer/form.html.twig',
            [
                'message' => $message,
                'registerForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/kunde/{firstname}-{surname}", name="showCustomer")
     * @param $firstname
     * @param $surname
     * @return Response
     */
    public function showCustomer($firstname, $surname): Response
    {
        $customer = $this->repository->findOneBy(['firstname' => $firstname, 'surname' => $surname]);

        return $this->render('customer/detail.html.twig', ['customer' => $customer]);
    }

    /**
     * @Route("/anzeigen", name="showCustomers")
     * @param BookingRepository $bookingRepository
     * @return Response
     */
    public function showCustomers(BookingRepository $bookingRepository): Response
    {
        $customers = $this->repository->findAll();

        /** @var Booking[] $customersWithBookings */
        $customersWithBookings = [];

        foreach ($customers as $customer) {
            $bookings = $bookingRepository->findBy(['customer' => $customer->getId()]);
            $customersWithBookings[] = $customer->setBookings($bookings);
        }

        return $this->render(
            'customer/table.html.twig',
            ['customers' => $customersWithBookings]
        );
    }

    /**
     * @param CustomerRepository|ObjectRepository $repository
     * @return CustomerController
     */
    public function setRepository(ObjectRepository $repository): CustomerController
    {
        $this->repository = $repository;

        return $this;
    }

}
