<?php

namespace App\Service;

use App\Entity\Customer;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;


final class CustomerValidator implements CustomerValidatorInterface
{
    /**
     * @ORM\Column(type="string")
     */
    private ObjectManager $objectManager;

    /**
     * CustomerValidator constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function exists($entity): bool
    {
        $searchCriteria = [
            'firstname' => $entity->getFirstname(),
            'surname' => $entity->getSurname(),
            'address' => $entity->getAddress()
        ];

        $result = $this->objectManager->getRepository(Customer::class)
            ->findOneBy($searchCriteria);

        if ($entity === $result) {
            return true;
        }

        return false;
    }

}