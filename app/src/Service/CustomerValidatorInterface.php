<?php

namespace App\Service;


/**
 * Interface ValidatorInterface
 * @package App\Service
 */
interface CustomerValidatorInterface
{
    /**
     * @param $entity
     * @return bool
     */
    public function exists($entity) : bool;


}