<?php

namespace App\Service;

use DateInterval;
use DateTime;
use DateTimeInterface;

/**
 * Class BookingValidator
 * @package App\Service
 */
final class BookingValidator implements BookingValidatorInterface
{
    /**
     * @var DateTime
     */
    private DateTime $currentDateTime;

    /**
     * BookingValidator constructor.
     */
    public function __construct()
    {
        $currentDateTime = new DateTime();
        $this->currentDateTime = $currentDateTime;
    }

    /**
     * @param DateTimeInterface $date
     * @return bool
     */
    public function isValidArrivalDate(DateTimeInterface $date): bool
    {
        $dateAddedTwoDays = $this->currentDateTime->add(new DateInterval('P2D'));

        return $dateAddedTwoDays < $date;
    }

    /**
     * @param DateTimeInterface $arrivalDate
     * @param DateTimeInterface $departureDate
     * @return bool
     */
    public function isValidDepartureDate(DateTimeInterface $arrivalDate, DateTimeInterface $departureDate): bool
    {
        return $arrivalDate < $departureDate;
    }

}