<?php


namespace App\Service;

use DateTime;
use DateTimeInterface;

/**
 * Interface BookingValidatorInterface
 * @package App\Service
 */
interface BookingValidatorInterface
{
    /**
     * @param DateTimeInterface $date
     * @return mixed
     */
    public function isValidArrivalDate(DateTimeInterface $date);

    /**
     * @param DateTimeInterface $arrivalDate
     * @param DateTimeInterface $departureDate
     * @return mixed
     */
    public function isValidDepartureDate(DateTimeInterface $arrivalDate, DateTimeInterface $departureDate);

}